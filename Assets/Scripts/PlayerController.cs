using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private MouseLook _mouseLook;
    [SerializeField] private PlayerMovement _playerMovement;

    public void SetAsPlayer()
    {
        _camera.gameObject.SetActive(true);
        _mouseLook.enabled = true;
        _playerMovement.enabled = true;
    }
}