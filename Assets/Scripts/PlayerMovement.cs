using Photon.Pun;
using UnityEngine;

public class PlayerMovement : MonoBehaviourPun
{
    [SerializeField] private CharacterController _characterController;

    [SerializeField] private float _speed = 12f;
    private Vector3 _velocity;

    private void Update()
    {
        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");

        var move = transform.right * x + transform.forward * z;

        _characterController.Move(move * (_speed * Time.deltaTime));
    }
}