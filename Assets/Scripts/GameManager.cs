﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private Button _shareButton;

    private void Awake()
    {
        CreateManagers();

        _shareButton.onClick.AddListener(JsInterface.StartCapture);
    }

    private void CreateManagers()
    {
    }

    private void Start()
    {
        Debug.Log("Connecting");
        PhotonNetwork.NickName = $"client_{Random.Range(0, 1000000).ToString()}";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log($"Connected. NickName == {PhotonNetwork.LocalPlayer.NickName}");
        PhotonNetwork.JoinOrCreateRoom("Room1", new RoomOptions(), TypedLobby.Default);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log($"Disconnected with cause {cause}");
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room created");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log($"Room creation failed: {message}");
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        var player = PhotonNetwork.Instantiate(_playerPrefab.name, new Vector3(0f, 1f, 0f), Quaternion.identity);
        player.GetComponent<PlayerController>().SetAsPlayer();
    }
}