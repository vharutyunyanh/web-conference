using Photon.Pun;
using UnityEngine;

public class MouseLook : MonoBehaviourPun
{
    [SerializeField] private float _sensitivity = 10f;

    [SerializeField] private Transform _body;
    [SerializeField] private Camera _camera;

    private float _xRotation;

    private void Start()
    {
        // Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        var mouseX = Input.GetAxis("Mouse X") * _sensitivity * Time.deltaTime;
        var mouseY = Input.GetAxis("Mouse Y") * _sensitivity * Time.deltaTime;

        _xRotation = Mathf.Clamp(_xRotation - mouseY, -90, 90f);

        _camera.transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
        _body.Rotate(Vector3.up * mouseX);
    }
}