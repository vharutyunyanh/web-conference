using System.Runtime.InteropServices;

public class JsInterface
{
    [DllImport("__Internal")]
    public static extern void Log(string str);

    [DllImport("__Internal")]
    public static extern void PrintFloatArray(float[] array, int size);

    [DllImport("__Internal")]
    public static extern int AddNumbers(int x, int y);

    [DllImport("__Internal")]
    public static extern string StringReturnValueFunction();

    [DllImport("__Internal")]
    public static extern void BindWebGLTexture(int texture);
    
    [DllImport("__Internal")]
    public static extern void StartCapture();
    
    [DllImport("__Internal")]
    public static extern void StopCapture();
}